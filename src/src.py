import os
import shutil
import numpy as np
import configparser
import json
from pyspark.ml.feature import HashingTF, IDF, IDFModel
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix
from pyspark import SparkContext, SparkConf
from pyspark.sql.session import SparkSession



def test_Trainer():
    trainer = Trainer()

def test_Processor():
    tst = Processor()


class Spark():

    def __init__(self) -> None:
        self.config = configparser.ConfigParser()
        self.config_path = os.path.join(os.getcwd(), 'config.ini')

        #приложение [настройка конфигураций]   
        self.spark_config = SparkConf()

        with open('src/spark_config.json', 'r') as spark_config:
            spark_configs = json.load(spark_config)

            for config in spark_configs:
                self.spark_config.set(config, spark_configs[config])
        
        self.num_parts = 40
        self.sc = None
        self.spark = None

    def get_context(self) -> SparkContext:
        if self.sc is None:
            try:
                self.sc = SparkContext(conf=self.spark_config)
            except:
                return
        return self.sc
    
    def get_session(self) -> SparkSession:
        
        if self.spark is None:
            try:
                self.spark = SparkSession(self.get_context())
            except:
                return
        return self.spark

class Trainer():

    def __init__(self) -> None:
        
        #инициализация
        self.config = configparser.ConfigParser()
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)
        pass

    def get_matrix(self, grouped, path='../models/films') -> bool:
        
        matrix = CoordinateMatrix(grouped.flatMapValues(lambda x: x).map(lambda x: MatrixEntry(x[0], x[1], 1.0)))
        matrix.entries.toDF().write.parquet(path)
        self.config["model"]["watched"] = path

    def _train_tf(self, grouped, path='./models/tf_model'):
        
        ##Создание TF-модели
    
        df = grouped.toDF(schema=["user_id", "movie_ids"])

        # Считаем TF для первых 100000 признаков
        hashingTF = HashingTF(inputCol="movie_ids", outputCol="rawFeatures", numFeatures=10000)
        tf_features = hashingTF.transform(df)

        hashingTF.write().overwrite().save(path)
        self.config["model"]["TF_PATH"] = path
        
        return tf_features

    def _save_idf_features(self, idf_features, path='./models/idf_features') -> bool:

        idf_features.write.format("parquet").save(path, mode='overwrite')
        self.config["model"]["IDF_FEATURES_PATH"] = path

    def _train_idf(self, tf_features, path='./models/idf_model') -> bool:
        
        ##Создание IDF-модели

        # Считаем IDF
        idf = IDF(inputCol="rawFeatures", outputCol="features")
        idf = idf.fit(tf_features)

        idf.write().overwrite().save(path)
        self.config["model"]["IDF_PATH"] = path
        
        # Для существующих пользователей
        idf_features = idf.transform(tf_features)
        if not self._save_idf_features(idf_features):
            return False

        return True

    def train_models(self) -> bool:

        sparkc = Spark()
        sc = sparkc.get_context()
        spark = sparkc.get_session()
        INPUT_FILENAME = self.config.get("DATA", "INPUT_FILE")

        # Группировка данных по пользовательским ID
        grouped = sc.textFile(INPUT_FILENAME, sparkc.num_parts) \
            .map(lambda x: map(int, x.split())).groupByKey() \
            .map(lambda x : (x[0], list(x[1])))

        # Считаем матрицу
        if not self.get_matrix(grouped):
            return False

        # Тренировка модели TF
        tf_features = self._train_tf(grouped)
        if tf_features is None:
            return False

        # Тренировка модели IDF
        if not self._train_idf(tf_features):
            return False

        # Апдейт конфигов
        os.remove(self.config_path)
        with open(self.config_path, 'w') as configfile:
            self.config.write(configfile)

        return True


class Processor():

    def __init__(self):
        
        #инициализация
        self.config = configparser.ConfigParser()
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)

        self.sparkc = Spark()
        self.sc = self.sparkc.get_context()
        self.spark = self.sparkc.get_session()
        

    def _load_watched(self) -> bool:
        #загрузка матрицы фильмов
        
        path = self.config.get("model", "watched")        
        self.watched = CoordinateMatrix(self.spark.read.parquet(path) \
            .rdd.map(lambda row: MatrixEntry(*row)))

    
    def _load_tf(self) -> bool:
        #загрузка TF модели
        path = self.config.get("model", "TF_PATH")
        self.hashingTF = HashingTF.load(path)
        
    def _load_idf(self) -> bool:
        #загрузка IDF модели
        path = self.config.get("model", "IDF_PATH")
        self.idf = IDFModel.load(path)
       
    def _load_idf_features(self) -> bool:
        #загрузка IDF признаков 
        path = self.config.get("model", "IDF_FEATURES_PATH")
        self.idf_features = self.spark.read.load(path)

    def _load(self) -> bool:
        #загрузчик
        self._load_watched()
        self._load_tf()
        self._load_idf()
        self._load_idf_features()
    
    def _get_recomendation(self, ordered_similarity, max_count=5) -> list:
        #считаем рекомендации (input --> ordered_similarity, output --> (movie_id, rank)))
        users_sim_matrix = IndexedRowMatrix(ordered_similarity)
        multpl = users_sim_matrix.toBlockMatrix().transpose().multiply(self.watched.toBlockMatrix())
        ranked_movies = multpl.transpose().toIndexedRowMatrix().rows.sortBy(lambda row: row.vector.values[0], ascending=False)

        result = []
        for i, row in enumerate(ranked_movies.collect()):
            if i >= max_count:
                break
            result.append((row.index, row.vector[0]))
        return result

    def sample(self):
        #рекомендации для пользователя
        temp_matrix = IndexedRowMatrix(self.idf_features.rdd.map(lambda row: IndexedRow(row["user_id"], Vectors.dense(row["features"]))))
        temp_block = temp_matrix.toBlockMatrix()
        similarities = temp_block.transpose().toIndexedRowMatrix().columnSimilarities()

        user_id = np.random.randint(low=0, high=self.watched.numCols())
        filtered = similarities.entries.filter(lambda x: x.i == user_id or x.j == user_id)
        ordered_similarity = filtered.sortBy(lambda x: x.value, ascending=False) \
            .map(lambda x: IndexedRow(x.j if x.i == user_id else x.i, Vectors.dense(x.value)))

        recomendations = self._get_recomendation(ordered_similarity)
        for movie_id, rank in recomendations:
            print(f'- movie # {movie_id} (rank: {rank})')

        return True

    def random(self):
        #рекомендации фильмов

        watched_movies = np.random.randint(low=0, high=self.watched.numCols(), size=int(self.watched.numCols()/4)).tolist()
        newdf = self.sc.parallelize([[-1, watched_movies]]).toDF(schema=["user_id", "movie_ids"])
        new_tf_features = self.hashingTF.transform(newdf)
        new_idf_features = self.idf.transform(new_tf_features)
        new_idf_features = new_idf_features.first()["features"]

        similarities = self.idf_features.rdd.map(
            lambda row: IndexedRow(
                row["user_id"],
                Vectors.dense(new_idf_features.dot(row["features"] / (new_idf_features.norm(2) * row["features"].norm(2))))
            )
        )
        ordered_similarity = similarities.sortBy(lambda x: x.vector.values[0], ascending=False)
        recomendations = self._get_recomendation(ordered_similarity)
        for movie_id, rank in recomendations:
            print(f'- movie # {movie_id} (rank: {rank})')


if __name__ == "__main__":
    trainer = Trainer()
    trainer.train_models()
    processor = Processor()
    processor.sample()
    processor.random()
